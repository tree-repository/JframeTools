# JframeTools（Jframe工具集合）
## 截图如下
#### apk安装
![image](https://gitee.com/tree-repository/JframeTools/blob/master/src/main/java/screenshots/apk_install.png)
#### 小工具
![image](https://gitee.com/tree-repository/JframeTools/blob/master/src/main/java/screenshots/tool.png)
#### monkey测试
![image](https://gitee.com/tree-repository/JframeTools/blob/master/src/main/java/screenshots/monkey.png)
#### 部分cmd
![image](https://gitee.com/tree-repository/JframeTools/blob/master/src/main/java/screenshots/cmd.png)
